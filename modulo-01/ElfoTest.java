import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXp()
    {
        Elfo elfo1 = new Elfo("Legolas");
        Dwarf dwarf1 = new Dwarf("Gimli");
        elfo1.atirarFlecha(dwarf1);
        assertEquals(1, elfo1.getExperiencia());
        assertEquals(3, elfo1.getQtdFlecha());
    }
    
    @Test
    public void atirarFlechaLimiteInferiorQuantidadeFlechas()
    {
        Elfo elfo1 = new Elfo("Legolas");
        Dwarf dwarf1 = new Dwarf("Gimli");
        for(int i=0; i<6; i++)
            elfo1.atirarFlecha(dwarf1);
        assertEquals(4, elfo1.getExperiencia());
        assertEquals(0, elfo1.getQtdFlecha());
    }
}
