import java.util.ArrayList;

public class Elfo
{
    private String nome;
    private int experiencia;
    
    private Item flecha = new Item(4, "Flecha");
    private Item arco = new Item(1, "Arco");
    
    public Elfo(String nome)
    {
        this.nome = nome;
        this.experiencia = 0;
    }
    
    public void setNome(String nome)
    {
        this.nome = nome;
    }
    
    public String getNome()
    {
        return this.nome;
    }
    
    public int getExperiencia()
    {
        return this.experiencia;
    }
       
    public Item getFlecha()
    {
        return this.flecha;
    }
    
    public int getQtdFlecha()
    {
        return this.flecha.getQuantidade();
    }
    
    public void aumentarXp()
    {
        this.experiencia++;
    }
    
    public void atirarFlecha(Dwarf anao)
    {
        int qtdAtual = flecha.getQuantidade();
        if(qtdAtual>0) {
            flecha.setQuantidade(qtdAtual - 1);
            anao.decrementaVida();
            aumentarXp();
        }
    }
    
}
