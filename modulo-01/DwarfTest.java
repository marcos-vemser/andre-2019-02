import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest
{
    @Test
    public void decrementaVidaDiminuiDezNaVida()
    {
        Dwarf dwarf1 = new Dwarf("Gimli");
        dwarf1.decrementaVida();
        assertEquals(100.0, dwarf1.getVida(),0.1);
    }
    
    @Test
    public void decrementaVidaLimiteInferiorQuantidadeVida()
    {
        Dwarf dwarf1 = new Dwarf("Gimli");
        for(int i=0; i<12; i++)
            dwarf1.decrementaVida();
        assertEquals(0, dwarf1.getVida(),0.1);
    }
}
