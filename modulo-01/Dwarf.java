public class Dwarf
{
    private String nome;
    private double vida;
    
    public Dwarf(String nome)
    {
        this.nome = nome;
        vida = 110.0;
    }
    
    public String getNome()
    {
        return this.nome;
    }
    
    public double getVida()
    {
        return this.vida;
    }
    
     public void decrementaVida()
    {
        this.vida = (this.vida>=10.0) ? this.vida-10.0 : this.vida;
    }  
    
}
